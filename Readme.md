#Read First
Accessing Product information by navigating through series of API's published thorugh BSL.

Its a tree of multiple layers, and here is a sample of navigating through this.

This can be used by the data team to load all the images for ranges, products, categories into the system.

# Installation

```sh
git clone git@bitbucket.org:sesa206167/seproductplayground.git
npm install
node index.js
```

It should generate the csv file to load into your org.
