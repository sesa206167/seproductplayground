var soap = require("soap");
var _ = require("lodash");
var fs = require("fs-extra");
var productOfferServiceClient = null;
var productOfferClient = null;

var ProductOfferurl =
  "http://oreo.schneider-electric.com/bsl-fo-service/ProductOfferServiceImplService?wsdl";
var productServiceUrl =
  "http://oreo.schneider-electric.com/bsl-fo-service/ProductService?wsdl";
var args = {
  scope: { brand: "Schneider Electric", country: "WW" },
  locale: { isoCountry: "US", isoLanguage: "en" }
};
var deargs = {
  scope: { brand: "Schneider Electric", country: "DE" },
  locale: { isoCountry: "DE", isoLanguage: "de" }
};

function prepareClients() {
  return new Promise((resolve, reject) => {
    soap.createClient(ProductOfferurl, function(err, client) {
      productOfferServiceClient = client;
      soap.createClient(productServiceUrl, function(err, client) {
        productOfferClient = client;
        resolve();
      });
    });
  });
}

function getAllCategoryIds() {
  return new Promise((resolve, reject) => {
    productOfferServiceClient.getCategoryTree(args, function(err, result) {
      // ..._.map(result.return, "id"),

      allcategories = _.map(
        _.flatten(_.map(result.return, "subCategories.category")),
        "id"
      );
      console.log(`Categories: ${allcategories.length}`);
      resolve(allcategories);
    });
  });
}

function getRangesForCategories(categoryIdlist) {
  return Promise.all(categoryIdlist.map(getRangesForCat));
}

function getRangesForCat(categoryId) {
  return new Promise((resolve, reject) => {
    productOfferServiceClient.getRangesOfCat(
      {
        ...deargs,
        categoryId
      },
      function(err, result) {
        resolve(_.get(result.return, "ranges"));
      }
    );
  });
}

function getProductsForRanges(rangesArray) {
  rangesArray = _.flatten(rangesArray);
  rangesIdArray = _.filter(_.map(rangesArray, "rangeId"));
  console.log(`Ranges : ${rangesIdArray.length}`);
  return Promise.all(rangesIdArray.map(getProductsPerRange));
}

function getProductsPerRange(rangeId) {
  return new Promise((resolve, reject) => {
    productOfferClient.getNodeTreeAndProductsFromRangeId(
      { ...args, rangeId, version: "6" },
      function(err, result) {
        allCommercialReferences = _.filter(
          _.flatten([
            ..._.map(result.NodeTreeBean, "commercialReferences"),
            ..._.map(
              _.flatten(
                _.map(
                  _.get(result, "NodeTreeBean.subNodes.node"),
                  "commercialReferences"
                )
              )
            )
          ])
        );
        allCommercialReferences = _.flatten(
          _.map(allCommercialReferences, "commercialReference")
        );

        resolve(allCommercialReferences);
      }
    );
  });
}

function getProductDetailsByCommercialReferences(commercialReferencearray) {
  console.log(`Commercial References: ${commercialReferencearray.length} `);
  return Promise.all(
    commercialReferencearray.map(getProductDetailByCommercialReference)
  );
}

function getProductDetailByCommercialReference(commercialRef) {
  return new Promise((resolve, reject) => {
    productOfferClient.getProductDetailByCommercialRef(
      { ...deargs, commercialRef, version: "6" },
      (err, result) => {
        var imageId = _.get(result, "ProductBean.pictureDocumentReference");
        var commercialReference = _.get(
          result,
          "ProductBean.commercialReference"
        );
        if (imageId) resolve(`${commercialReference},${imageId}`);
        else resolve();
      }
    );
  });
}

prepareClients()
  .then(getAllCategoryIds)
  .then(getRangesForCategories)
  .then(getProductsForRanges)
  .then(getProductDetailsByCommercialReferences)
  .then(res => {
    res = _.filter(res);
    console.log(`Products: ${res.length}`);
    fs.writeFileSync("productsToLoad.csv", _.join(res, "\n"));
  })
  .catch(console.error);
